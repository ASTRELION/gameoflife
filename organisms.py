from abc import ABC, abstractmethod
from enum import Enum
import random
import sys

class OrganismType(Enum):

    DEAD = -sys.maxsize
    FAUNA = 1 # animal
    FLORA = 2 # plant


class Organism(ABC):

    def __init__(self, type = OrganismType.DEAD, icon: str | list[str] = "") -> None:

        self.type = type
        if isinstance(icon, list):
            self.icon = icon
        else:
            self.icon = [icon]


    def get_icon(self) -> str:

        return random.choice(self.icon)


    @abstractmethod
    def result(self, neighbors: list["Organism"]) -> "Organism":
        """Determine the outcome of this organism based on its neighbors
        """
        raise NotImplementedError


class Dead(Organism):

    def __init__(self) -> None:

        super().__init__(OrganismType.DEAD, "    ")


    def result(self, neighbors: list[Organism]) -> Organism:

        alive = aliveOf(neighbors)
        trees = organismOf(neighbors, Tree)
        seeds = organismOf(neighbors, Seedling)

        if trees == 3 or (alive == 3 and trees > 1):
            return Seedling()

        return self


class Seedling(Organism):

    def __init__(self) -> None:

        super().__init__(OrganismType.FLORA, "🌱")


    def result(self, neighbors: list[Organism]) -> Organism:

        alive = aliveOf(neighbors)
        if alive < 1 or alive > 3:
            return Dead()

        return Tree()


class Tree(Organism):

    def __init__(self) -> None:

        super().__init__(OrganismType.FLORA, ["🌳", "🌲"])


    def result(self, neighbors: list[Organism]) -> Organism:

        alive = aliveOf(neighbors)
        trees = organismOf(neighbors, Tree)

        if trees < 2:
            return Dead()
        elif trees > 3:
            return Mushroom()

        return self


class Mushroom(Organism):

    def __init__(self) -> None:

        super().__init__(OrganismType.FLORA, "🍄")


    def result(self, neighbors: list[Organism]) -> Organism:

        alive = aliveOf(neighbors)
        mushrooms = organismOf(neighbors, Mushroom)

        if alive < 1 or alive > 3:
            return Dead()

        return self


def aliveOf(organisms: list[Organism]) -> int:
    """Get the amount of alive Organisms in a given list
    """
    return sum([o.type.value > 0 for o in organisms])


def deadOf(organisms: list[Organism]) -> int:
    """Get the amount of dead Organisms in a given list
    """
    return sum([o.type.value <= 0 for o in organisms])


def floraOf(organisms: list[Organism]) -> int:
    """Get the amount of flora Organisms in a given list
    """
    return sum([o.type == OrganismType.FLORA for o in organisms])


def faunaOf(organisms: list[Organism]) -> int:
    """Get the amount of fauna Organisms in a given list
    """
    return sum([o.type == OrganismType.FAUNA for o in organisms])


def organismOf(organisms: list[Organism], type: Organism) -> int:

    return sum([isinstance(o, type) for o in organisms])
