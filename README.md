# Game of Life

Simple [Mastodon][mastodon] bot that simulates a game inspired by [Conway's Game of Life][gameOfLife] and [Forest Bot][forestBot].

## Rules

- The game's initial state is seeded with the account follower's names
- A new game is started when there is no life left or when two units of time are the same
- Specific organism rules are defined in [`organisms.py`](./organisms.py)

## Contributing

The best way to contribute would be to add additional organisms and rulesets to [`organisms.py`](./organisms.py). To add a new organism:

1. Add a new class to [`organisms.py`](./organisms.py) that implements `Organism`
2. Implement the `result()` function
3. Edit the `result()` function of `Dead` to include your organism, if it can be created from surrounding organisms
4. Edit the `result()` function of other organisms, if created by the result of an organism



[forestBot]: https://mastodon.social/@forestbot@hellsite.site
[gameOfLife]: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
[mastodon]: https://joinmastodon.org
