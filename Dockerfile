FROM python:3.10-alpine

ADD *.py /
ADD data/.token.example /data/.token
ADD requirements.txt /

RUN pip install -r /requirements.txt

CMD ["python", "/__main__.py"]
