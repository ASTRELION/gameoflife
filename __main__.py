from mastodon import Mastodon, StreamListener, MastodonError, MastodonAPIError
from time import sleep
import random
import copy

from organisms import Organism, OrganismType, Tree, Dead, Seedling

mastodon: Mastodon = None

class GameOfLife:

    def __init__(self, width: int, height: int) -> None:

        self.width = width
        self.height = height
        self.board: list[Organism] = []
        self.last_board: list[Organism] = []
        self.last_post = 0
        self.follow_count = 0
        self.favorites = 0
        self.reblogs = 0
        self.replies = 0
        self.growth = 0
        self.i = 0


    def neighbors(self, index):
        """Get the cells around the given index
        """
        arr = []
        row = index // self.width
        col = index % self.width
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == 0 and j == 0:
                    continue
                r = row + i
                c = col + j
                if r >= 0 and r < self.height and c >= 0 and c < self.width:
                    arr.append(self.board[(r * self.height) + c])

        return arr


    def start(self, callback):

        seed = random.random()
        print(f"Using seed: {seed}")
        random.seed(seed)
        self.last_board.clear()
        self.board.clear()
        for i in range(self.width * self.height):
            self.board.append(Seedling() if random.random() > 0.75 else Dead())
        prefix = "New life is sprouting!\n"

        while True:

            # grow new life if the account is growing/being interacted with
            if mastodon:
                self.follow_count = len(mastodon.fetch_remaining(mastodon.account_followers(mastodon.me())))
                if self.last_post:
                    last_post = mastodon.status(self.last_post)
                    self.favorites = last_post["favourites_count"]
                    self.reblogs = last_post["reblogs_count"]
                    self.replies = last_post["replies_count"]

                growth = (self.follow_count * 1.5) + (self.reblogs * 1.2) + (self.replies * 1.1) + (self.favorites * 1)
                chance = max(0, min(0.5, (growth - self.growth) / self.follow_count))
                print(f"Growth chance: {chance}")
                if self.growth != 0:
                    for i, b in enumerate(self.board):
                        if b.type.value < 0 and random.random() < chance:
                            self.board[i] = Seedling()
                self.growth = growth

            # check for stale game
            if all([o.type == OrganismType.DEAD for o in self.board]):
                print("There is no life left")
                return
            elif len(self.last_board) == len(self.board):
                same = True
                for i, b in enumerate(self.board):
                    if self.board[i].type != self.last_board[i].type:
                        same = False
                        break

                if same:
                    print("Game entered a cycle")
                    return

            # progress game
            print(f"Time: {self.i}")
            print(self)
            callback(self, prefix)
            prefix = ""

            board_copy = copy.deepcopy(self.board)
            for i, b in enumerate(self.board):
                board_copy[i] = b.result(self.neighbors(i))
            self.last_board = copy.deepcopy(self.board)
            self.board = board_copy
            self.i += 1
            sleep(60 * 60) # 1 hour


    def __str__(self) -> str:

        string = ""
        for i, b in enumerate(self.board):
            if i != 0 and i % self.width == 0:
                string += "\n"
            string += b.get_icon()

        return string


def post(game: GameOfLife, prefix = ""):

    if mastodon:
        game.last_post = mastodon.status_post(prefix + str(game))
    print("Posting")


def main():

    global mastodon
    mastodon = Mastodon(access_token="data/.token")
    game_of_life = GameOfLife(11, 11)
    while True:
        game_of_life.start(post)


if __name__ == "__main__":

    main()
